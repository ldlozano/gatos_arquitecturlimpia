package com.eafit.petfinder.busqueda.repositorios;

import com.eafit.petfinder.busqueda.entidades.Mascota;

public interface RepositorioBusqueda {

	Mascota obtenerMascota(String idMascota);

}
