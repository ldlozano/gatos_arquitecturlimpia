package com.eafit.petfinder.busqueda.entidades;

public class Ubicacion {
	private Float latitud;
	private Float longitud;
	
	public Ubicacion(Float latitud, Float longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
	}
	
	public Float getLatitud() {
		return latitud;
	}

	public Float getLongitud() {
		return longitud;
	}


	@Override
	public boolean equals(Object obj) {
		Ubicacion comparado = (Ubicacion) obj;
		if(comparado == null) return false;
		
		if(comparado.getLatitud().equals(this.getLatitud()) 
				&& comparado.getLongitud().equals(this.getLongitud())){
			return true;
		}
		
		return false;
	}
}
