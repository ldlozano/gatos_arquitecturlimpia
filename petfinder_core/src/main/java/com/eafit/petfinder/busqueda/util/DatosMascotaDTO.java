package com.eafit.petfinder.busqueda.util;

import com.eafit.petfinder.busqueda.entidades.Ubicacion;

public class DatosMascotaDTO {
	private String idMascota;
	private Ubicacion ubicacion;
	
	public String getIdMascota() {
		return idMascota;
	}
	public void setIdMascota(String idMascota) {
		this.idMascota = idMascota;
	}
	public Ubicacion getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}
}
