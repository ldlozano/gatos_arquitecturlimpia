package com.eafit.petfinder.busqueda.entidades;

import java.util.UUID;

public class Mascota {
	private String id;
	private String nombre;
	private String tipo;
	private Cuidador dueno;
	private static final double RADIO_TIERRA = 6372.8;
	
	public Mascota() {
		UUID uuid = UUID.randomUUID();
		this.setId(uuid.toString());
	}
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Cuidador getDueno() {
		return dueno;
	}
	public void setDueno(Cuidador dueno) {
		this.dueno = dueno;
	}

	public Float calcularDistanciaDueno(Ubicacion ubicacionActual) {
        double dLat = Math.toRadians(ubicacionActual.getLatitud() - dueno.getUbicacion().getLatitud());
        double dLon = Math.toRadians(ubicacionActual.getLongitud() - dueno.getUbicacion().getLongitud());
        double latitudDuenoRadianes = Math.toRadians(dueno.getUbicacion().getLatitud());
        double latitudMascotaRadianes = Math.toRadians(ubicacionActual.getLatitud());
 
        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) 
        	* Math.cos(latitudDuenoRadianes) * Math.cos(latitudMascotaRadianes);
        double c = 2 * Math.asin(Math.sqrt(a));
        return new Float(RADIO_TIERRA * c);
	}
	
}
