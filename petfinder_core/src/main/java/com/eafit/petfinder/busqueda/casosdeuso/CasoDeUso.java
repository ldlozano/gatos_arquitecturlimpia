package com.eafit.petfinder.busqueda.casosdeuso;

public interface CasoDeUso<ParametroEntrada, Resultado> {
   public Resultado ejecutar(ParametroEntrada entrada);
}
