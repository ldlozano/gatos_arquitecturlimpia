package com.eafit.petfinder.busqueda.casosdeuso;

import com.eafit.petfinder.busqueda.entidades.Mascota;
import com.eafit.petfinder.busqueda.repositorios.RepositorioBusqueda;
import com.eafit.petfinder.busqueda.util.DatosMascotaDTO;

public class ObtenerDistanciaCasoDeUso implements CasoDeUso<DatosMascotaDTO, Float> {
	RepositorioBusqueda repositorio;

	public Float ejecutar(DatosMascotaDTO entrada) {
		Mascota mascota = repositorio.obtenerMascota(entrada.getIdMascota());
		Float distanciaCalculada = mascota.calcularDistanciaDueno(entrada.getUbicacion());
		return distanciaCalculada;
	}

}
