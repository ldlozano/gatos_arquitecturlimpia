package com.eafit.petfinder.busqueda.entidades;

import java.util.UUID;

public class Cuidador {
	private String id;
	private String nombre;
	private Ubicacion ubicacion;
	private String telefono;
	private String correo;
	
	public Cuidador() {
		UUID uuid = UUID.randomUUID();
		this.setId(uuid.toString());
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Ubicacion getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	@Override
	public boolean equals(Object obj) {
		Cuidador comparado = (Cuidador) obj;
		if(comparado == null){
			return false;
		}
		if(this.getId().equals(comparado.getId())){
			return true;
		}
		return false;
	}
	
}
