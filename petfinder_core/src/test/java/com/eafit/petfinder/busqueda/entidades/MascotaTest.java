package com.eafit.petfinder.busqueda.entidades;

import static org.junit.Assert.*;

import org.junit.Test;

public class MascotaTest {

	@Test
	public void testCalcularDistancia() {
		Mascota mascotica = new Mascota();
		Cuidador cuidadorcito = new Cuidador();
		
		Ubicacion ubicacion = new Ubicacion(new Float(10.5), new Float(-75.5));
		cuidadorcito.setUbicacion(ubicacion);
		mascotica.setDueno(cuidadorcito);
		
		Ubicacion ubicacionActual = new Ubicacion(new Float(64.5), new Float(-75.5));
		Float distancia = mascotica.calcularDistanciaDueno(ubicacionActual);
		assertEquals(new Float(6006.2227), distancia);
		
		
	}

}
